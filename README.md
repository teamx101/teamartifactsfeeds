#Artifacts=========================Azure DevOps Feeds================================================================
#====================Package Feeds to provide our teams with the ability to control our packages and their dependencies=======================

#============================From Project Artifact, Create New Feed======================================================


#======================Add Feed to List of Package Sources==============================================================

- Connect to Feed, e.g Nuget
- Copy the nuget url
- Click on Options from Visual Studio, Under Nuget Package Settings or Right click on the solutionn.
- From Nuget Package Manager, Select Package Sources,
- Remote nuget.org so we have our package as first, we can add nuget.org as upstream
- click + plus to the source url, on the Name portion type e.g NugetFeeds , then click update. Note: Ensure you give it a proper name
- finally click Ok.

#Next
#===========================Run a build and we add package in the Feed====================================================
#==================Add task to our build pipeline to upload packages to team feeds continually=========================================
- Ensure a build pipeline is already setup
- Click Edit  and plus + to add a Task
- Search for Task and Select Nuget, then Add (Note: this should be placed after build processes, so you can drag and move it up if its not currently in that position)
- Change the command* to Push
- Target Feed* Select the Feed to be used
- Click on Run to start a build or A Code Commit will initial the build afterwards (or Save and Queue)

#Alternatively
# Download the nuget.exe into the project directory and publish package to source using the two below commands

 ./nuget.exe pack TeamArtifactsFeeds.csproj

./nuget.exe push -source "DotnetTeamArtifacts" -ApiKey VSTS TeamArtifactsFeeds.1.0.0.nupkg

#===============Feeds, with Upstream============================Helps protect against public package provider outages================



#======================Consume Packages from Feeds into new solutions=====================================================
- Browse for Nuget Package from Visual Studio Nuget Package Manager (you should see DotnetTeamArtifacts) or
- Install-Package TeamArtifactsFeeds -version 1.0.0

#=====================Add Teams to Feed to, Add Packages or Add to the Feed=================================================
- 
-